\chapter{Packing Your Bag}
\label{ch:packing_your_bag}

\epigraph{Every thunderstorm begins with a single drop. Try to be this drop.}
         {Lorenzo Orsetti\supercite{lorenzo-orsetti}}

\noindent
The previous chapters covered a great deal of equipment in an attempt to provide discussion about most of the items medics carry to actions.
You do not need to acquire everything that was listed in order to be an effective medic.
Most injuries are minor, and a major part of being a medic is providing emotional care to patients (something you need no equipment for).

Knowing what to bring and what to leave behind is an important skill to develop.
On one hand, you might be tempted to be prepared for every possible situation and overpack.
On the other, you might think ``I don't really need this'' and leave important equipment behind.
This chapter will discuss ways to balance what to bring to actions.

\section*{Avoid Posturing}

When you are considering what equipment to acquire, consider whether you are making your choices based on what is the most useful for your goals or whether you are trying to become some idealized version of an Elite Riot Medic Warrior.
Tacticool, a portmanteau of ``tactical'' and ``cool,'' is a derisive term used to describe equipment that looks cool but is unnecessary or not fit for purpose.
It is not uncommon to see tacticool medics who have far too much personal protective equipment, are massively over-equipped, and yet manage to be wildly ineffective in their duties.

You do not need an abundance of equipment to be a useful medic.
You do not need to impress anyone with your gear.
Acquire a minimal amount of actually useful equipment to start, and acquire more as you need it.
Don't be tacticool.
Be practicool.\footnotemark[1]

\footnotetext[1]{
Imagine me doing overly sarcastic finger guns while saying this, please.
}

\section*{Your Bag}
\index{medic bags|(}
\index{backpacks|(}

Your medic bag should be something you can run and move quickly with.
Simple backpacks or messenger bags are generally good choices.
Bags that are worn over one shoulder may make it difficult to run.
Carrying a bag in your hand is impractical and inadvisable.

EMS backpacks (\autoref{fig:ems_backpack}) typically have one main interior compartment, possibly with a large divider.
They unzip all the way and open up like a clamshell so that their entire contents are visible and accessible at once.
The equipment inside is divided into smaller pouches with clear plastic fronts so that their contents are visible.
These pouches are usually color-coded and labelled to help a medics quickly find what they are looking for.
Pouches are often divided into a single responsibility such as bleeding control or CPR.

\begin{figure}[htbp]
\centering
\caption{EMS Backpack\supercite{baedr}}
\label{fig:ems_backpack}
\includesvg[width=\textwidth, height=6cm, keepaspectratio]{ems-backpack-open}
\end{figure}

You do not need to acquire an EMS backpack, but your bag should attempt to emulate the features of an EMS bag.
This style of bag is used by EMS around the world because it is effective at helping medics quickly render care.
A school or hiking backpack plus a few pouches will typically suffice, and these can all be easily or cheaply acquired.

Using these features will make it easier for you to find what you are looking for, and moreover it will make it easier for your buddy or a bystander to help you find something.
When your bag is well organized, you will know where everything is, and you can direct someone to hand you what you need by saying ``Tourniquet. In the large red pouch.''

Commonly used items such as examination gloves, a few gauze pads, trauma shears, and medical tape should be easily accessible.
Medics will often put these into vest pockets or use a small hip or waist bag.
\acrshort{MOLLE}-compatible equipment can be used with small pouches for commonly used items.

\subsection*{Clean Bags}

A clean bag is a bag that never has anything put into it that you wouldn't bring to an action.
This may mean never putting weapons, recreational drugs, or private documents into it.
As a medic, you may be stopped and searched, and you do not want to have forgotten to remove something from your bag.
Use of a clean bag is a protection against accidents.

Even if it is illegal to bring weapons to a demonstration, you still may choose to do so for self defense.
In this case, putting weapons into your clean bag doesn't violate the rule because it is something you have chosen to do knowing the risks.

Having a dedicated bag for your medic gear is recommended in general.
It makes packing easier since all of your gear is already packed from the previous action.
Having a dedicated bag is a prerequisite for having a clean bag.

\index{backpacks|)}
\index{medic bags|)}

\section*{Packing Considerations}

Your bag has finite space, so you will have to prioritize its contents based on what you expect to use.

\triplesubsection{Self care}
You should always bring water and food for yourself.
You cannot help other people if you become weak and exhausted from not eating all day.
Your food should be in sealed containers or packaging to prevent contamination from riot control agents or body substances.
Sunblock and extra layers of clothing may be appropriate depending on the weather.

\triplesubsection{Don't weigh yourself down}
You need to be able to keep pace with a moving action.
This may just be marching all day, but it may also mean running, climbing stairs, and hoping fences.
You will needlessly tire yourself out if your bag is too heavy.
If you cannot keep up with an action, you cannot help anyone.
Additionally, you are more agile with a lighter bag and this may help you evade arrest.

\triplesubsection{Take what you know how to use}
If you have not trained with equipment, don't bring it to an action.
A stressful situation is the wrong time to learn to use new equipment.
Leave it at home until you are comfortable using it.

\triplesubsection{Prioritize for common injuries}
Common injuries are simple wounds (cuts and scrapes), dehydration, low blood glucose, and riot control agent contamination.
Pack for these injuries first.
Specialized equipment for less commonly seen injuries takes up space that is usually better used to care for patients you will more frequently encounter.
What constitutes common will by region, expected opposition, and weather.
For example, you may want to bring extra emergency blankets in cold weather.

\triplesubsection{Pack for many patients}
Many actions require little to no medical support, but you may also have many patients on any given day.
You should have enough gauze and bandages to treat multiple people.

\triplesubsection{Pack what you can afford to lose}
While at an action, your equipment may be damaged, stolen, or confiscated.
Consider the frequency with which police confiscate things from protesters before bringing hard to acquire or expensive equipment to an action.

\triplesubsection{Have eyewash easily accessible}
Your eyewash bottle should be easily accessible so that you can self-treat in the event you get pepper spray or other riot control agents in your eyes.

\triplesubsection{Pack for known teams}
Not every medic needs to carry a full loadout of all available gear.
Generally one medic per team can carry a large bag with less common equipment, and each medic carries a basic bag with gauze, medical tape, and examination gloves in the appropriate size.

\triplesubsection{Consider omitting PPE}
Not every action is going to be tear gassed, and a large respirator can be overly conspicuous and take up a great deal of space.
For actions where you don't expect tear gas or significant pepper spray, consider sticking only swimming goggles and a filtering half-mask respirator in a vest pocket.

\section*{Example Bags}

The following example medic bags are references to help make the guidelines discussed above a bit more concrete.
Only medical supplies are covered in these equipment lists.
A medic's own food, water, and other gear is assumed to be included.

\triplesubsection{Medic Light}
The Medic Light kit is a simple medical kit that fits easily into a small pouch.
It has only the basics to help with unexpected injuries at an otherwise uneventful demonstration.
Medics attending actions where they are not actively on duty as a medic might also carry this bag.
Affinity groups may have a dedicated medic who carries a similar loadout.

\triplesubsection{Medic Standard}
The Medic Standard kit is more comprehensive than the Medic Light.
Medics who are less experienced might carry bags like this due to it lacking more advanced diagnostic equipment.
Experienced medics might also choose to carry a bag like this if they are concerned with arrest\index{arrest!medics@of medics} and equipment confiscation in a repressed protest environment.

\triplesubsection{Medic Heavy}
The Medic Heavy kit contains more diagnostic equipment than the Medic Standard kit as well as greater quantities and varieties of equipment for treating wounds.
A bag like this is often carried by experienced medics or uniformed medics.\footnotemark[2]
Due the size, weight, and total value of equipment inside, it is not recommend to carry a bag like this when there is high amounts of repression\index{repression} against medics.
On a given team, only one medic needs to carry a heavy bag.

\footnotetext[2]{
Medics carrying a bag like this are likely EMS personnel and are certified to use more advanced medical equipment.
These items were intentionally omitted.
}

\subsection*{Packing List}

The ``Packing List`` tables contain the contents that would be carried in Medic Light, Medic Standard, and Medic Heavy bag.
These loadouts are labelled by \textbf{L}, \textbf{S}, and \textbf{H} respectively.
Quantities in \textit{italics} are optional items.

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Wound Care}
\centering
\begin{tabularx}{\linewidth}{rrr|X}
    \multicolumn{1}{c|}{\textbf{L}} &
        \multicolumn{1}{c|}{\textbf{S}} &
        \multicolumn{1}{c|}{\textbf{H}} &
        \multicolumn{1}{c}{\textbf{Item}} \\
    \hline
    5            & 10           & 20             & Individual package gauze (10 $\times$ \SI{10}{\cm}) \\
                 & 5            & 10             & Gauze roll \SI{10}{\cm} \\
                 & 1            & 2              & Non-adhesive dressing (10 $\times$ \SI{10}{\cm}) \\
                 & 1            & 2              & Roll \SI{2}{\cm} medical tape \\
                 &              & 1              & Roll \SI{5}{\cm} medical tape \\
                 &              & 2              & Elastic net dressing \\
                 & 2            & 3              & Self-adhering bandage (\SI{2}{\cm}) \\
                 & 1            & 2              & Self-adhering bandage (\SI{5}{\cm}) \\
                 & 1            & 2              & Elastic bandage \\
                 &              & 2              & Combat dressing \\
                 & \textit{1}   & 1              & Chest seal (pair) \\
                 & \textit{1}   & 1              & Package hemostatic gauze \\
                 & 3            & 5              & Package wound closure strips \\
    10           & 20           & 20             & Adhesive bandages (assorted size) \\
                 & 1            & 2              & Triangle bandage \\
                 & 1            & 1 (\textit{2}) & Tourniquet \\
                 &              & \textit{1}     & Junctional tourniquet \\
                 & \textit{1}   & 1              & Burn dressing (10 $\times$ \SI{10}{\cm}) or burn gel (\SI{50}{\mL}) \\
    \SI{50}{\mL} & \SI{50}{\mL} & \SI{100}{\mL}  & Antiseptic spray \\
                 & \SI{50}{\mL} & \SI{50}{\mL}   & Antibacterial creme \\
                 &              & \SI{400}{\mL}  & Antiseptic irrigation solution \\
                 &              &  2             & Irrigation syringe (\SI{30}{\mL}) \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- First Aid}
\centering
\begin{tabularx}{\linewidth}{rrr|X}
    \multicolumn{1}{c|}{\textbf{L}} &
        \multicolumn{1}{c|}{\textbf{S}} &
        \multicolumn{1}{c|}{\textbf{H}} &
        \multicolumn{1}{c}{\textbf{Item}} \\
    \hline
               & 1          & 1          & Trauma shears \\
    1          & 3          & 5          & Emergency blanket \\
               & 2          & 3          & Instant cold compress \\
               & 1          & 2          & Vomit bag \\
    3          & 5          & 10         & Dextrose \& salt drink mix packet \\
    2          & 5          & 5          & Single-use saline vials \\
    \textit{1} & 1          & 1          & \SI{500}{\mL} pneumatic eyewash bottle \\
               & 1          & 2          & Tweezers \\
               &            & \textit{1} & Carry tarp \\
               & \textit{1} & 1          & Splint \\
               &            & 1          & Cervical collar \\
               & \textit{1} & \textit{1} & Can refrigerant spray \\
               & \textit{5} & \textit{5} & Paracetamol tablets \\
               & \textit{5} & \textit{5} & Ibuprofen tablets \\
               & \textit{5} & \textit{5} & Aspirin tablets \\
               & \textit{5} & \textit{5} & Anti-histamine tablets \\
               & \textit{1} & \textit{1} & Salbutamol inhaler w/ spacer \\
               & \textit{1} & \textit{1} & Epinephrine autoinjector \\
               & \textit{1} & \textit{1} & Tooth preservation kit \\
               & \textit{1} & \textit{1} & Narcan spray bottle \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Infection Control}
\centering
\begin{tabularx}{\linewidth}{rrr|X}
    \multicolumn{1}{c|}{\textbf{L}} &
        \multicolumn{1}{c|}{\textbf{S}} &
        \multicolumn{1}{c|}{\textbf{H}} &
        \multicolumn{1}{c}{\textbf{Item}} \\
    \hline
    5            & 10            & 15            & Pair Examination gloves \\
    \SI{30}{\mL} & \SI{100}{\mL} & \SI{100}{\mL} & Hand sanitizer \\
                 & 1             & 2             & Surgical mask \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Basic Life Support}
\centering
\begin{tabularx}{\linewidth}{rrr|X}
    \multicolumn{1}{c|}{\textbf{L}} &
        \multicolumn{1}{c|}{\textbf{S}} &
        \multicolumn{1}{c|}{\textbf{H}} &
        \multicolumn{1}{c}{\textbf{Item}} \\
    \hline
    1 & 1 & 1 & Foldable CPR mask \\
      & 1 & 1 & Pocket CPR mask (or BVM) \\
      &   & 1 & Magill forceps \\
      &   & 1 & Manual suction pump \\
      & 2 & 2 & Disposable razors \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Diagnostic Equipment}
\centering
\begin{tabularx}{\linewidth}{rrr|X}
    \multicolumn{1}{c|}{\textbf{L}} &
        \multicolumn{1}{c|}{\textbf{S}} &
        \multicolumn{1}{c|}{\textbf{H}} &
        \multicolumn{1}{c}{\textbf{Item}} \\
    \hline
    & 1 & 1 & Pulse oximeter \\
    & 1 & 1 & Penlight \\
    &   & 1 & Stethoscope \\
    &   & 1 & Blood pressure cuff \\
    &   & 1 & Blood glucose meter (with lancets and test strips) \\
    &   & 1 & Thermometer (with disposable covers) \\
\end{tabularx}
\end{table}

\begin{table}[htbp]
\footnotesize
\caption{Packing List --- Misc.}
\centering
\begin{tabularx}{\linewidth}{rrr|X}
    \multicolumn{1}{c|}{\textbf{L}} &
        \multicolumn{1}{c|}{\textbf{S}} &
        \multicolumn{1}{c|}{\textbf{H}} &
        \multicolumn{1}{c}{\textbf{Item}} \\
    \hline
    \textit{1} & 1  & 2  & Package tissues \\
               & 5  & 5  & Tampons (assorted sizes) \\
               & 1  & 1  & Roll black duct tape \\
               & 3  & 5  & Plastic trash bags \\
               & 5  & 10 & Pair earplugs \\
               & 5  & 10 & Safety pins \\
\end{tabularx}
\end{table}

\section*{IFAKs}
\index{individual first aid kit|see {IFAK}}
\index{first aid!kit, individual|see {IFAK}}
\index{IFAK|(}

An \acrshort{IFAK} (\acrlong{IFAK}) is a small first aid kit carried by anyone, not just medics, that contains basic medical supplies that will help in the treatment of life-threatening injuries.
An IFAK differs from what we might call a ``personal first aid kit'' in that it is minimal and lacks niceties like plasters or analgesics.
It only has equipment for short-term interventions in serious injuries.
IFAKs are carried in a visible location (usually the hip or thigh, sometimes the chest) and are clearly labeled usually with the text ``IFAK,'' a cross, or your region's equivalent medical symbol.
Sometimes these labels are phosphorescent (glow-in-the-dark) to aid in their discovery in low-light settings.
Like with your medic bag, IFAKs should open clam-shell style to make it easy to quickly access all their contents.

\begin{figure}[htbp]
\centering
\caption{IFAK\supercite{bizhan}}
\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includesvg[height=3.5cm, keepaspectratio]{ifak-closed}
    \vspace{1.4cm}
\end{subfigure}
\begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includesvg[height=6cm, keepaspectratio]{ifak-open}
\end{subfigure}
\end{figure}

With the rise in far-right violence around the world from vehicle attacks to mass shootings, some individuals and groups recommend carrying IFAKs not just to demonstrations or other conflicts but also as part of one's ``every-day carry.''
In particular, the George Floyd Insurrection\index{George Floyd Insurrection} in the US sparked large campaigns by anarchists to assemble and distribute IFAKs to front-line protesters.
Wide deployment of IFAKs can help save lives, and if you expect significant violence in your region, you should consider offering IFAK training, facilitating their distribution, and normalizing their presence at actions.

Medics may carry an IFAK in addition to their main medic bag.
If you do this, when treating a patient, always pull equipment from your main bag before pulling equipment from your IFAK.
The IFAK is for \textit{you} first.
You might stash your bag to stage a rescue, give it to your buddy, or simply remove it while treating someone.
If you get injured while separated from your bag, an empty IFAK does you no good.
Always treat patients from your main bag, then use your IFAK as an emergency reserve.

When treating someone, if you notice they have an IFAK, use the supplies from their IFAK first.
This prevents depletion of your own supplies so that you may treat other patients with serious injuries later during the action.
This is especially true in mass casualty incidents\index{mass casualty incidents}.
If you do not immediately see an IFAK, do not waste time looking for one.
If you find one after the patient has been stabilized or awaiting transit, consider replenishing any equipment you used from their IFAK.

A suggested IFAK packing list can be found in \autoref{tab:ifak}.
Some of the items listed here are not listed in the loadouts of the main medic bags, but they are included in the IFAK.
This is because the expectation is that someone more qualified will be treating you and will have the knowledge necessary for how to use these additional items.

\begin{table}[htbp]
\footnotesize
\caption{IFAK Packing List}
\label{tab:ifak}
\centering
\begin{tabularx}{\linewidth}{r|X}
    \textbf{Quantity} & \textbf{Item} \\
    \hline
    1               & Examination gloves (pair) \\
    1               & Trauma shears \\
    1               & Small permanent marker \\
    1               & Foldable CPR mask \\
    1               & Emergency blanket \\
    1 \textit{(2)}  & Tourniquet \\
    1               & Combat dressing \\
    1 \textit{(2)}  & Package z-fold hemostatic gauze \\
    1               & Chest seal (pair) \\
    1               & Oropharyngeal or nasopharyngeal airway (in your size, with lubricant) \\
    \textit{1}      & Chest decompression needle (TPAK) \\
    \textit{(1--2)} & Chem light (glow stick) \\
    \textit{if applicable} & Personal medication (e.g., anticonvulsants, epinephrine) with dosage and instructions \\
\end{tabularx}
\end{table}

\index{IFAK|)}

\section*{Summary}

The three fundamental questions to ask yourself when packing your bag are:

\begin{enumerate}
    \item What do I actually know how to use?
    \item What am I realistically going to need?
    \item What amount of weight can I actually carry?
\end{enumerate}

There are, of course, many other considerations, but if you can answer these questions, chances are you will be prepared for most injuries and illnesses you encounter.
